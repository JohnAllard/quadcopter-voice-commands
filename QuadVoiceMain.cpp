#include <WinSock2.h> 
#include <iostream> 
#include <string> 
#include "VoiceCommand.h" 
  
//Function Prototypes 
SOCKET connectToController(char*, char*); 
  
int main(int argc, char* argv[]) { 
    // Ensure that the IP address and port were passed as command-line arguments 
    if (argc != 3) { 
        std::cerr << "Missing IP address and/or port." << std::endl; 
        return 1; 
    } 
  
    // Get the socket that will be used to send commands to controller 
    std::cout << "Establishing connection to " << argv[1] << std::endl; 
    SOCKET commandSocket = connectToController(argv[1], argv[2]); 
  
    if (commandSocket == NULL) { 
        std::cerr << "Error in establishing the connection." << std::endl; 
        return 1; 
    } 
  
    // Disable the output of keys to the console 
    // from: http://stackoverflow.com/questions/1413445/read-a-password-from-stdcin 
    HANDLE stdinHandle = GetStdHandle(STD_INPUT_HANDLE); 
    DWORD mode; 
    GetConsoleMode(stdinHandle, &mode); 
    mode &= ~ENABLE_ECHO_INPUT; 
    SetConsoleMode(stdinHandle, mode); 
  
    // Begin the actual voice command program 
    try { 
        VoiceCommand voxCommand(&commandSocket, argv[0]); 
        voxCommand.startVoiceCommand(); 
    } 
    catch (ConfigNullException &e) { 
        //There was an error with opening the models and dictionary. 
        std::cerr << e.what() << std::endl; 
    } catch (DecoderNullException &e) { 
        //There was an error with creating the decoder 
        std::cerr << e.what() << std::endl; 
    } 
  
    // Close down socket and clean up connection 
    closesocket(commandSocket); 
    WSACleanup(); 
  
    return 0; 
} 
  
/** Connects the program to the IP address and port passed through the command-line. It handles all of the errors 
* that may occur. 
* 
* @param ipAddress          The IP address of the controller 
* @param port               The port to connect to at the given IP address 
* @return                   Returns the socket if connection was successful, else it returns NULL 
**/
SOCKET connectToController(char* ipAddress, char* port) { 
    WSAData version;                                    // Used to determine the version of the Ws2_32.dll. Needs to be at least version 2.2 
    WORD vers = MAKEWORD(2,2);                          // Make a WORD version of 2.2 so it can be compared with "version" 
      
    int versionCompare = WSAStartup(vers, &version);    // Find out if the Ws2_32.dll is the correct version 
    if (versionCompare != 0) {                          // The version is not at least 2.2 
        // Print error message and what caused it. 
        std::cout << "The version of Ws2_32.dll is not at least 2.2 -\n" << WSAGetLastError() << std::endl; 
        return NULL; 
    } 
  
    // Create the socket. AF_INET = IPv4, SOCK_STREAM = TCP byte stream, IPPROTO_TCP = using TCP 
    SOCKET result = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP); 
    // Check that socket was opened correctly 
    if (result == INVALID_SOCKET) { 
        std::cout << "Error creating socket." << std::endl; 
        return NULL; 
    } 
  
    // Add address info to the socket. 
    sockaddr_in address; 
    address.sin_family = AF_INET;                           // Sets the socket type, IPv4 
    address.sin_addr.S_un.S_addr = inet_addr(ipAddress);    // Address that socket will connect to 
    u_short prt = u_short(atoi(port));                       
    address.sin_port = htons(prt);                          // Port to connect to. 
  
    // Setup Complete. Connect to the controller 
    int connectionResult = connect(result, (sockaddr*)&address, sizeof(address)); 
    // Check for an error when connecting. If there is one, then close the socket 
    if (connectionResult == SOCKET_ERROR) { 
        std::cout << "Error when attempting connection." << std::endl; 
        closesocket(result); 
        WSACleanup(); 
        return NULL; 
    } 
  
    // Connection was succesfully established 
    return result; 
}